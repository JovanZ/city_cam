package com.jofo.android.city_cam;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;

public class TakePicActivity extends AppCompatActivity {

    private String mPathPic;

    private Bitmap imageOreintationValidator(Bitmap bitmap, String path) {

        ExifInterface ei;
        try {
            ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = rotateImage(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = rotateImage(bitmap, 180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = rotateImage(bitmap, 270);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    private Bitmap rotateImage(Bitmap source, float angle) {

        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                    matrix, true);
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
        }
        return bitmap;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_pic);
        Intent i = getIntent();
        mPathPic = i.getStringExtra("takePic");
        File mPhotoFile = new File(mPathPic);

        if (mPhotoFile.exists()) {
            Bitmap mBitmapPic = BitmapFactory.decodeFile(mPhotoFile.getAbsolutePath());
            ImageView mPicView = (ImageView) findViewById(R.id.picture_view);
            mBitmapPic = imageOreintationValidator(mBitmapPic, mPathPic);
            mPicView.setImageBitmap(mBitmapPic);
        }

        Button mButtonSave = (Button) findViewById(R.id.save_button);
        Button mButtonRetake = (Button) findViewById(R.id.retake_button);
        Button mButtonCancel = (Button) findViewById(R.id.cancel_button);

        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent saveIntent = new Intent();
                setResult(RESULT_OK, saveIntent);
                finish();
            }
        });

        mButtonRetake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent retakeIntent = new Intent();
                setResult(RESULT_FIRST_USER, retakeIntent);
                finish();
            }
        });

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cancelIntent = new Intent();
                setResult(RESULT_CANCELED, cancelIntent);
                finish();
            }
        });
    }
}
