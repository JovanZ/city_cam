package com.jofo.android.city_cam;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class CityCamActivity extends AppCompatActivity {

    private static final int WRITE_TAG = 1;
    private static final int PHOTO_TAG = 2;
    private static final int LOG_TAG = 3;
    private static final int MAP_TAG = 4;
    private Menu mMenu;
    private boolean loggedIn;
    private SharedPreferences sp;
    private String mLogName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_cam);

        sp = getSharedPreferences("saved", Context.MODE_PRIVATE);

        mLogName = sp.getString("name", "");
        loggedIn = sp.getBoolean("log", false);
        if (!mLogName.equals("")) {
            String str = getString(R.string.hello) + ": " + mLogName;
            Toast toast = Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG);
            toast.show();
        }

        RelativeLayout map_button = (RelativeLayout) findViewById(R.id.loc_rel_layout);
        map_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CityCamActivity.this, MapsActivity.class);
                startActivityForResult(i, MAP_TAG);
            }
        });
        RelativeLayout photo_button = (RelativeLayout) findViewById(R.id.photo_button);
        photo_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CityCamActivity.this, PhotoActivity.class);
                startActivityForResult(i, PHOTO_TAG);
            }
        });

        RelativeLayout write_button = (RelativeLayout) findViewById(R.id.relativeLayout2);
        write_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CityCamActivity.this, WriteActivity.class);
                startActivityForResult(i, WRITE_TAG);
            }
        });

        RelativeLayout send_button = (RelativeLayout) findViewById(R.id.send_button);
        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog pd = new ProgressDialog(CityCamActivity.this);
                pd.setMessage(getString(R.string.send_text));
                pd.show();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pd.dismiss();
                        Toast toast = Toast.makeText(getApplicationContext(), getString(R.string.send_done), Toast.LENGTH_LONG);
                        toast.show();
                    }
                }, 2000);
            }
        });

        ImageView mAdvert = (ImageView) findViewById(R.id.advert);
        mAdvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iA = new Intent();
                iA.setAction(Intent.ACTION_VIEW);
                iA.addCategory(Intent.CATEGORY_BROWSABLE);
                iA.setData(Uri.parse("http://hyperether.com/"));
                startActivity(iA);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == WRITE_TAG) {
            if (resultCode == RESULT_OK) {
                String str = data.getStringExtra("writeText");
                Toast toast = Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG);
                toast.show();
            }
        }
        if (requestCode == PHOTO_TAG) {
            if (resultCode == RESULT_OK) {
                String str = data.getStringExtra("savePic");
                Toast toast = Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG);
                toast.show();
            }
        }
        if (requestCode == LOG_TAG) {
            if (resultCode == RESULT_OK) {
                mLogName = data.getStringExtra("log_name");
                if (!mLogName.equals("")) {
                    String str = getString(R.string.hello) + ": " + mLogName;
                    Toast toast = Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG);
                    updateMenuTitles();
                    toast.show();
                }
            }
        }
        if (requestCode == MAP_TAG) {
            if (resultCode == RESULT_OK) {
                String str = data.getStringExtra("mapText");
                Toast toast = Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor mEditor = sp.edit();

        mEditor.putString("name", mLogName);
        mEditor.putBoolean("log", loggedIn);

        mEditor.apply();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.city_cam_menu, menu);
        this.mMenu = menu;
        updateMenuTitles();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.log_in:
                loggedIn = true;
                Intent intent_log = new Intent(CityCamActivity.this, LogActivity.class);
                startActivityForResult(intent_log, LOG_TAG);
                break;
            case R.id.log_out:
                loggedIn = false;
                updateMenuTitles();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void updateMenuTitles() {
        MenuItem loginMenuItem = mMenu.findItem(R.id.log_in);
        MenuItem logoutMenuItem = mMenu.findItem(R.id.log_out);

        if (loggedIn) {
            loginMenuItem.setVisible(false);
            logoutMenuItem.setVisible(true);
        } else {
            mLogName = "";
            logoutMenuItem.setVisible(false);
            loginMenuItem.setVisible(true);
        }
    }
}
