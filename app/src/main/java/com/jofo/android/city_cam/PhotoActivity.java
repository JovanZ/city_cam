package com.jofo.android.city_cam;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotoActivity extends AppCompatActivity {
    private static final int SELECT_PICTURE = 1;
    private static final int REQUEST_TAKE_PHOTO = 2;
    private static final int RESULT_PIC = 3;
    private static final String TAG = "PhotoActivity";
    private String selectedImagePath;
    private String mCurrentPhotoPath;


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES + "/city_cam");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        Button mLoadPic = (Button) findViewById(R.id.load_pic);
        mLoadPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // in onCreate or any event where your want the user to
                // select a file
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), SELECT_PICTURE);
            }
        });

        Button mTakePic = (Button) findViewById(R.id.take_pic);
        mTakePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case SELECT_PICTURE:
                if (resultCode == RESULT_OK) {
                    Uri selectedImageUri = data.getData();
                    selectedImagePath = getPath(selectedImageUri);

                    Intent load_pic = new Intent();
                    setResult(RESULT_OK, load_pic);
                    load_pic.putExtra("savePic", selectedImagePath);
                    finish();
                }
                break;
            case REQUEST_TAKE_PHOTO:
                if (resultCode == RESULT_OK) {
                    Intent take_pic = new Intent(PhotoActivity.this, TakePicActivity.class);
                    take_pic.putExtra("takePic", mCurrentPhotoPath);
                    startActivityForResult(take_pic, RESULT_PIC);
                }
                break;
            case RESULT_PIC:
                switch (resultCode) {
                    case RESULT_OK:
                        Intent i = new Intent();
                        i.putExtra("savePic", mCurrentPhotoPath);
                        setResult(RESULT_OK, i);
                        finish();
                        break;
                    case RESULT_CANCELED:
                        File file = new File(mCurrentPhotoPath);
                        file.delete();
                        break;
                    default:
                        File file1 = new File(mCurrentPhotoPath);
                        file1.delete();
                        dispatchTakePictureIntent();
                        break;
                }
            default:
        }
    }

    /**
     * helper to retrieve the path of an image URI
     */
    public String getPath(Uri contentUri) {
        String path = contentUri.toString().substring(7);

        try {
            Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
            String document_id = "";

            try {
                if (cursor != null && cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    document_id = cursor.getString(0);
                    document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
                }
            } catch (Exception e) {

            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
            cursor = getContentResolver().query(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
            try {
                if (cursor != null && cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                }
            } catch (Exception e) {

            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        } catch (Exception e) {
        }
        return path;
    }
}
