package com.jofo.android.city_cam;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class WriteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write);


        Button ok_button = (Button) findViewById(R.id.ok_button);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ok_intent = new Intent();
                setResult(RESULT_OK, ok_intent);
                String mEdit = ((EditText) findViewById(R.id.write_text)).getText().toString();
                ok_intent.putExtra("writeText", mEdit);
                finish();
            }
        });
    }
}
